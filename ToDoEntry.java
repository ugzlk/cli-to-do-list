import java.time.Duration;

public class ToDoEntry {
	public int position;
	public String title;
	public boolean done;
	public boolean hidden;
	public boolean constant;
	public boolean collapsed;
	public String details;
	public Duration timeRequired;
	public Duration timeSpent;
	public String deadline;
	public int counter;
	public int maxCounter;

	public ToDoEntry(int pos, String title, boolean done) {
		this.position = pos;
		this.title = title;
		this.done = done;
		this.hidden = false;
		this.constant = false;
		this.collapsed = true;
		this.details = "";
		this.timeRequired = Duration.ZERO;
		this.timeSpent = Duration.ZERO;
		this.deadline = "";
		this.counter = -1;
		this.maxCounter = -1;
	}

	public String toString(boolean showPosition) {
		String doneMarker = (this.done ? "X" : " ");
		if (constant)
			doneMarker = "-";

		if (showPosition) {
			return this.position + ". " + "[" + doneMarker + "] " + this.title;
		} else {
			return "[" + doneMarker + "] " + this.title;
		}
	}
}
