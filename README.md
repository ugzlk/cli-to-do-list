# CLI To-Do List
### This project provides command-line interface To-Do-Lists and includes a manager for said to-do lists.

Switch to branch **build** and download **ToDo.jar** for a working build.  
Then execute the jar with **java -jar ToDo.jar**.  

Or clone this repository and build the executable jar with **jar cfe ToDo.jar ToDoMenu \*.class**.  
Once started you are greeted by the list manager.  

