import java.util.*;
import java.io.*;
import java.util.stream.Collectors;
import java.time.Duration;
import java.text.DecimalFormat;

public class ToDo {

    private static boolean showPos = true;
    private static boolean showTimePercentage = true;
    private static int posGap = 1;

    private static final int standardTitleLength = 9;
    private static final int minEntryLength = 4;

    private static final DecimalFormat percentageFormatter = new DecimalFormat("#.#");

    public static void run(String fileName, Scanner userInput) {
        if (fileName.length() == 0) {
            Use.printMessage("Please specify an existing file or a new file that should be used for storing data.\nExiting...");
            return;
        }

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true));

            ToDoList toDoList = Save.buildToDoListFromFile(fileName, showPos, showTimePercentage, posGap);

            if (toDoList == null) {
                bw.close();
                return;
            }

            showPos = toDoList.showPositions;
            showTimePercentage = toDoList.showTimePercentage;;
            posGap = toDoList.positionGap;

            Use.printMessage("Type help for a list of all commands.");

            while (true) {
                displayList(toDoList.list, fileName);

                String commandInput = userInput.next();
                Command command;
                try {
                    command = Use.parseCommand(commandInput);
                } catch (IllegalArgumentException e) {
                    Use.printMessage("Unknown command!\nType help for a list of all commands.");
                    continue;
                }

                String entry = userInput.nextLine();
                Scanner entryScanner = new Scanner(entry);
                Scanner cleanEntryScanner = new Scanner(entry);
                var refEntries = findAllReferencedEntries(toDoList.list, entryScanner);

                switch (command) {
                    case EXIT:
                        cleanEntryScanner.close();
                        entryScanner.close();
                        bw.close();
                        return;

                    case SAVE:
                        Use.deleteContentOfFile(fileName);
                        bw.write(Save.createToDoListSaveString(toDoList.list, showPos, showTimePercentage, posGap));
                        bw.flush();
                        Use.printMessage("Saved current list.");
                        break;

                    case HELP:
                        help();
                        break;

                    case ADD:
                        ToDoEntry entryToAdd = new ToDoEntry(toDoList.list.size(), entry.strip(), false);
                        toDoList.list.add(findFirstHiddenIndex(toDoList.list), entryToAdd);
                        updateToDoEntryPositions(toDoList.list);
                        break;

                    case DEL:
                        toDoList.list.removeAll(refEntries);
                        updateToDoEntryPositions(toDoList.list);
                        break;

                    case MARK:
                        for (ToDoEntry entryToMark : refEntries)
                            entryToMark.done = !entryToMark.done;
                        break;

                    case MOVE:
                        try {
                            int initialPos = cleanEntryScanner.nextInt();
                            int finalPos = cleanEntryScanner.nextInt();
                            moveToDoEntry(toDoList.list, initialPos, finalPos);
                        } catch (Exception e) {
                            Use.printMessage("Invalid positions.");
                        }
                        break;

                    case SET:
                        if (entryScanner.hasNext()) {
                            String newTitle = entryScanner.nextLine();

                            for (ToDoEntry entryToSet : refEntries) {
                                entryToSet.title = newTitle.strip();
                            }
                        } else {
                            Use.printMessage("No new title specified.");
                        }
                        break;

                    case DET:
                        if (!cleanEntryScanner.hasNextInt()) {
                            Use.printMessage("Please specify a position first.");
                            break;
                        }
                        int detPos = cleanEntryScanner.nextInt();
                        if (toDoList.list.size()-1 < detPos) {
                            printOutOfBoundsMessage(detPos);
                            break;
                        }

                        ToDoEntry entryToDet = toDoList.list.get(detPos);

                        String details;
                        details = openDetailsDialog(entryToDet);
                        entryToDet.details = details;
                        Use.printMessage("Saved details for To-Do entry " + entryToDet.position + ".");
                        break;

                    case CLEAR:
                        for (ToDoEntry entryToClear : refEntries) {
                            entryToClear.details = "";
                        }
                        break;

                    case POS:
                        if (cleanEntryScanner.hasNextInt()) {
                            posGap = cleanEntryScanner.nextInt();
                        } else {
                            showPos = !showPos;
                        }
                        break;

                    case EXT:
                        for (ToDoEntry entryToExt : refEntries) {
                            entryToExt.collapsed = false;
                        }
                        break;

                    case COLL:
                        for (ToDoEntry entryToColl : refEntries) {
                            entryToColl.collapsed = true;
                        }
                        break;

                    case HIDE:
                        for (ToDoEntry entryToHide : refEntries) {
                            entryToHide.hidden = true;
                        }

                        toDoList.list = moveHiddenEntriesToEnd(toDoList.list);
                        break;

                    case UNHIDE:
                        for (ToDoEntry entryToUnhide : toDoList.list) {
                            entryToUnhide.hidden = false;
                        }
                        break;

                    case TIME:
                        Duration dur = getTimeRequiredFromInput(entryScanner);
                        for (ToDoEntry entryToTime : refEntries) {
                            entryToTime.timeRequired = dur;
                        }
                        break;

                    case SPENT:
                        Duration durSpent = getTimeRequiredFromInput(entryScanner);
                        for (ToDoEntry entryToTime : refEntries) {
                            entryToTime.timeSpent = durSpent;
                        }
                        break;

                    case PERCENT:
                        showTimePercentage = !showTimePercentage;
                        break;

                    case CONST:
                        for (ToDoEntry entryToConst : refEntries) {
                            entryToConst.constant = !entryToConst.constant;
                        }
                        break;

                    case ADDTIME:
                        var duration = getTimeRequiredFromInput(entryScanner);
                        for (var refEntry : refEntries) {
                            var durations = new ArrayList<Duration>();
                            durations.add(duration);
                            durations.add(refEntry.timeSpent);
                            refEntry.timeSpent = sumDurations(durations);
                        }
                        break;

                    case SEARCH:
                        if (entryScanner.hasNext("\".*\"")) {
                            var searchInput = entryScanner.next();
                            var searchText = searchInput.substring(1, searchInput.length()-1);
                            var filteredEntries = toDoList.list.stream().filter(x -> x.title.contains(searchText)).toList();
                            toDoList.list.forEach(x -> x.hidden = true);
                            filteredEntries.forEach(x -> x.hidden = false);

                            StringBuilder sb = new StringBuilder("Matched entries: ");
                            for (ToDoEntry e : filteredEntries) {
                                sb.append(e.position).append(" ");
                            }
                            Use.printMessage(sb.substring(0));

                        } else {
                            Use.printMessage("Unable to parse search text.");
                        }
                        break;

                    case DEAD:
                        if (entryScanner.hasNext()) {
                            var deadline = entryScanner.nextLine();

                            for (ToDoEntry entryToSet : refEntries)
                                entryToSet.deadline = deadline.strip();
                        } else {
                            for (ToDoEntry entryToSet : refEntries)
                                entryToSet.deadline = "";
                        }
                        break;

                    case COUNT:
                        if (entryScanner.hasNextInt()) {
                            var counter = entryScanner.nextInt();
                            System.out.println("mama: " + counter);

                            for (ToDoEntry entryToSet : refEntries)
                                entryToSet.counter = counter;
                        } else {
                            for (ToDoEntry entryToSet : refEntries)
                                entryToSet.counter = -1;
                        }
                        break;

                    case MAXCOUNT:
                        if (entryScanner.hasNextInt()) {
                            var maxCounter = entryScanner.nextInt();

                            for (ToDoEntry entryToSet : refEntries)
                                entryToSet.maxCounter = maxCounter;
                        } else {
                            for (ToDoEntry entryToSet : refEntries)
                                entryToSet.maxCounter = -1;
                        }
                        break;

                    default:
                        Use.printMessage("Unknown command!\nType help for a list of all commands.");
                        break;
                }
                entryScanner.close();
                cleanEntryScanner.close();
            }
        } catch (IOException e) {
            Use.printMessage("Unable to create file with name \"" + fileName + "\"\nExiting...");
        }
    }

    private static void help() {
        System.out.println("exit                        [save before exiting if you want to keep your changes]");
        System.out.println("save");
        System.out.println("add {title}");
        System.out.println("del {id}                    [delete entries]");
        System.out.println("set {id} - {title}          [set new entry titles]");
        System.out.println("mark {id}                   [mark entries as done/undone]");
        System.out.println("const {id}                  [mark entries as constant/variable]");
        System.out.println("move {id} {position}        [move entry to position]");
        System.out.println("pos                         [toggle position display]");
        System.out.println("pos {x}                     [only show every xth position]");
        System.out.println("percent                     [show completion percentages]");
        System.out.println("det {id}                    [add details, type '$%$' in a new line to save, only applicable to one entry at a time]");
        System.out.println("clear {id}                  [clear details]");
        System.out.println("ext {id}                    [extend details]");
        System.out.println("coll {id}                   [collapse details]");
        System.out.println("hide {id}                   [hide entries]");
        System.out.println("unhide                      [unhide all hidden entries]");
        System.out.println("search \"text\"               [search by text, hide all non-matched]");
        System.out.println("time {id} {duration}        [set required time]");
        System.out.println("spent {id} {duration}       [set spent time]");
        System.out.println("addtime {id} {duration}     [add spent time]");
        System.out.println("dead {id} - {text}          [specify deadline]");
        System.out.println("count {id} - {integer}      [specify counter]");
        System.out.println("maxcount {id} - {integer}   [specify max counter]");
        Use.printMessage("Values for {id}: 0 1 2 3 (positions) | * (all including hidden) | range 0 100 (position range) | marked | unmarked");
        Use.printMessage("Values for {duration}: hours 5 | minutes 6 | seconds 7 | hours 5 minutes 6 seconds 7");
        Use.printMessage("Examples:\nadd Run errands\nmove 0 3\ndel *\nhide range 5 30\ntime 1 2 minutes 15");
    }

    private static void displayList(List<ToDoEntry> toDoList, String fileName) {
        System.out.println();

        if (toDoList.isEmpty()) {
            Use.printMessage("List is empty...");
        } else {
            Use.printListHeader(fileName, standardTitleLength);
            int lengthOfLastIndex = Use.getNumberOfDigits(toDoList.size()-1);
            int lengthOfLongestTitle = getLengthOfLongestVisibleTitle(toDoList);

            for (int i = 0; i < toDoList.size(); i++) {
                ToDoEntry entryToDisplay = toDoList.get(i);
                if (entryToDisplay.hidden)
                    continue;

                int lengthOfCurrentIndex = Use.getNumberOfDigits(i);
                int indexIndentationDiff = lengthOfLastIndex - lengthOfCurrentIndex;

                if (showPos) {
                    Use.printIndentation(indexIndentationDiff);

                    if ((posGap > 1) && (i % posGap != 0)) {
                        Use.printIndentation(lengthOfCurrentIndex + 2);
                        System.out.print(entryToDisplay.toString(false));
                    } else {
                        System.out.print(entryToDisplay.toString(true));
                    }
                } else {
                    System.out.print(entryToDisplay.toString(false));
                }

                displayEntryInfo(entryToDisplay, lengthOfLongestTitle, lengthOfLastIndex);

                System.out.println();
            }
            var hiddenCount = toDoList.stream().filter(x -> x.hidden).count();
            if (hiddenCount > 0)
                System.out.println("...");
        }
        System.out.println();
    }

    private static void displayEntryDetails(ToDoEntry entry, int indentation) {
        Use.printDetailsSeparator(entry.title.length(), indentation);

        Scanner detailsScanner = new Scanner(entry.details);

        while (detailsScanner.hasNextLine()) {
            Use.printIndentation(indentation);
            System.out.println(detailsScanner.nextLine());
        }

        Use.printDetailsSeparator(entry.title.length(), indentation);
    }

    private static void moveToDoEntry(List<ToDoEntry> toDoList, int initialPos, int finalPos) {
        if (initialPos < 0 || initialPos >= toDoList.size()
            || finalPos < 0 || finalPos >= toDoList.size()) {
            throw new IllegalArgumentException();
        }

        ToDoEntry tmp = toDoList.get(initialPos);
        ToDoEntry other = toDoList.get(finalPos);

        if (tmp.hidden || other.hidden)
            throw new IllegalArgumentException();

        toDoList.remove(tmp);
        toDoList.add(finalPos, tmp);
        updateToDoEntryPositions(toDoList);
    }

    private static String openDetailsDialog(ToDoEntry entry) {
        Use.printMessage("Write details for To-Do entry " + entry.position + ":");

        System.out.print(entry.details);

        StringBuilder resultBuilder = new StringBuilder(entry.details);
        Scanner detailsInput = new Scanner(System.in);
        String lastLine = detailsInput.nextLine();

        while (!lastLine.equals("$%$")) {
            resultBuilder.append(lastLine).append("\n");
            lastLine = detailsInput.nextLine();
        }
        return resultBuilder.substring(0);
    }

    private static List<ToDoEntry> findAllReferencedEntries(List<ToDoEntry> toDoList, Scanner entryScanner) {
        List<ToDoEntry> entriesFound = new ArrayList<>();

        while (entryScanner.hasNext()) {
            if (entryScanner.hasNextInt()) {
                int nextPos = entryScanner.nextInt();
                addEntryFromListToListByIndex(toDoList, entriesFound, nextPos);
            } else if (entryScanner.hasNext("\\*")) {
                entriesFound = toDoList;
                entryScanner.next();
                break;
            } else if (entryScanner.hasNext("marked")) {
                var markedEntries = toDoList.stream().filter(x -> x.done).toList();
                entriesFound.addAll(markedEntries);
                entryScanner.next();
            } else if (entryScanner.hasNext("unmarked")) {
                var unmarkedEntries = toDoList.stream().filter(x -> !x.done).toList();
                entriesFound.addAll(unmarkedEntries);
                entryScanner.next();
            } else if (entryScanner.hasNext("range")) {
                entryScanner.next();
                if (entryScanner.hasNextInt()) {
                    int start = entryScanner.nextInt();
                    if (entryScanner.hasNextInt()) {
                        int end = entryScanner.nextInt();
                        for (int i = start; i <= end; i++) {
                            addEntryFromListToListByIndex(toDoList, entriesFound, i);
                        }
                    }
                }
            } else {
                break;
            }
        }

        if (entryScanner.hasNext("-"))
            entryScanner.next("-");

        List<ToDoEntry> result = entriesFound.stream().distinct().collect(Collectors.toList());
        StringBuilder sb = new StringBuilder("Referenced entries: ");
        for (ToDoEntry e : result) {
            sb.append(e.position).append(" ");
        }
        Use.printMessage(sb.substring(0));
        return result;
    }

    private static void addEntryFromListToListByIndex(List<ToDoEntry> fromList, List<ToDoEntry> toList, int index) {
        if (fromList.size() > index) {
            ToDoEntry entry = fromList.get(index);
            if (!entry.hidden)
                toList.add(entry);
        }
    }

    private static void printOutOfBoundsMessage(int position) {
        Use.printMessage("Position " + position + " out of bounds.");
    }

    private static List<ToDoEntry> moveHiddenEntriesToEnd(List<ToDoEntry> entries) {
        List<ToDoEntry> result = new ArrayList<>(entries);
        List<ToDoEntry> hiddenEntries = new ArrayList<>();

        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).hidden) {
                hiddenEntries.add(result.remove(i));
                i--;
            }

        }

        result.addAll(hiddenEntries);

        updateToDoEntryPositions(result);

        return result;
    }

    private static void updateToDoEntryPositions(List<ToDoEntry> entries) {
        entries.forEach(e -> e.position = entries.indexOf(e));
    }

    private static int findFirstHiddenIndex(List<ToDoEntry> entries) {
        Optional<ToDoEntry> firstHiddenEntry = entries.stream().filter(e -> e.hidden).findFirst();

        return firstHiddenEntry.map(toDoEntry -> toDoEntry.position).orElseGet(entries::size);
    }

    private static Duration getTimeRequiredFromInput(Scanner entryScanner) {
        Duration result = Duration.ZERO;
        if (entryScanner.hasNext("hours")) {
            entryScanner.next();
            if (entryScanner.hasNextInt()) {
                int hours = entryScanner.nextInt();
                result = result.plusHours(hours);
            }
        }

        if (entryScanner.hasNext("minutes")) {
            entryScanner.next();
            if (entryScanner.hasNextInt()) {
                int minutes = entryScanner.nextInt();
                result = result.plusMinutes(minutes);
            }
        }

        if (entryScanner.hasNext("seconds")) {
            entryScanner.next();
            if (entryScanner.hasNextInt()) {
                int seconds = entryScanner.nextInt();
                result = result.plusSeconds(seconds);
            }
        }

        return result;
    }

    private static String getDisplayedTime(ToDoEntry entry, int lengthOfLongestTitle) {
        var result = "";
        int indentation = lengthOfLongestTitle - entry.title.length();

        if (!entry.timeRequired.equals(Duration.ZERO)) {
            result += Use.getIndentation(indentation);
            String timeReqStr = entry.timeRequired.toString().substring(2).toLowerCase();
            String timeSpentStr = entry.timeSpent.toString().substring(2).toLowerCase();
            if (showTimePercentage) {
                float percentage = (float)entry.timeSpent.toSeconds() / entry.timeRequired.toSeconds() * 100;
                String percentageStr = percentageFormatter.format(percentage) + "%";
                result += " [" + timeSpentStr + "/" + timeReqStr + "] " + percentageStr;
            } else {
                result += " [" + timeSpentStr + "/" + timeReqStr + "]";
            }
        }
        return result;
    }

    private static String getDisplayedDeadline(ToDoEntry entry, int lengthOfLongestTitle) {
        var result = "";
        int indentation = lengthOfLongestTitle - entry.title.length();

        if (entry.deadline.length() > 0) {
            result += Use.getIndentation(indentation) + " [" + entry.deadline + "]";
        }

        return result;
    }

    private static String getDisplayedCounter(ToDoEntry entry, int lengthOfLongestTitle) {
        var result = "";
        int indentation = lengthOfLongestTitle - entry.title.length();

        if (entry.counter >= 0) {
            result += Use.getIndentation(indentation);
            result += " [" + entry.counter;
            if (entry.maxCounter >= 0)
                result += "/" + entry.maxCounter;
            result += "]";
        }
        return result;
    }

    private static void displayEntryInfo(ToDoEntry entry, int lengthOfLongestTitle, int lengthOfLastIndex) {
        int offset = 2;
        if (!entry.details.isEmpty()) {
            if (entry.collapsed) System.out.print(" >");
            else System.out.print(" <");
            offset = 0;
        }

        var info = "";
        info += getDisplayedTime(entry, lengthOfLongestTitle + offset);
        info += getDisplayedDeadline(entry, info.isEmpty() ? lengthOfLongestTitle + offset : 0);
        info += getDisplayedCounter(entry, info.isEmpty() ? lengthOfLongestTitle + offset : 0);
        System.out.print(info);

        if (!entry.details.isEmpty()) {
            if (!entry.collapsed) {
                System.out.println();
                int detailsIndentation = showPos ? lengthOfLastIndex + 2 + minEntryLength : minEntryLength;
                displayEntryDetails(entry, detailsIndentation);
            }
        }
    }

    private static int getLengthOfLongestVisibleTitle(List<ToDoEntry> toDoList) {
        int result = 0;

        for (ToDoEntry entry : toDoList) {
            if (entry.hidden)
                continue;

            int nextLength = entry.title.length();
            if (nextLength > result)
                result = nextLength;
        }

        return result;
    }

	private static Duration sumDurations(List<Duration> durations) {
		var result = Duration.ZERO;
		for (var dur : durations) {
			result = result.plus(dur);
		}

		return result;
	}
}
