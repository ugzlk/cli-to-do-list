import java.util.ArrayList;
import java.util.List;

public class ToDoList {
    protected List<ToDoEntry> list;
    protected boolean showPositions;
    protected boolean showTimePercentage;
    protected  int positionGap;

    public ToDoList(boolean pShowPositions, boolean pShowTimePercentage, int pPositionGap) {
        list = new ArrayList<>();
        showPositions = pShowPositions;
        showTimePercentage = pShowTimePercentage;
        positionGap = pPositionGap;
    }
}
