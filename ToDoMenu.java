import java.util.*;
import java.io.*;

public class ToDoMenu {
    private static final int standardTitleLength = 9;

    public static void main(String[] args) {
        // Add new list (specify name) -> file is created with same name in working directory
        // Delete list (specify pos)
        // Edit list name -> filename will also be changed
        // Execute list


        var mainFileName = "main.txt";
        var mainFile = new File(mainFileName);
        List<String> toDoLists = new ArrayList<>();

        try {
            if (mainFile.createNewFile()) {
                Use.printMessage("File created: " + mainFile.getName());
            } else {
                Use.printMessage("File already exists: " + mainFile.getName());
                toDoLists = buildListFromFile(mainFile);
            }
        } catch (IOException e) {
            System.out.println("An IOException occurred.");
            e.printStackTrace();
        }

        Use.printMessage("Type help for a list of all commands.");

        Scanner userInput = new Scanner(System.in);

        while (true) {
            displayList(toDoLists);

            String commandInput = userInput.next();
            Command command;
            try {
                command = Use.parseCommand(commandInput);
            } catch (IllegalArgumentException e) {
                Use.printMessage("Unknown command!\nType help for a list of all commands.");
                continue;
            }

            String entry = userInput.nextLine();
            Scanner entryScanner = new Scanner(entry);

            switch (command) {
                case EXIT -> {
                    userInput.close();
                    entryScanner.close();
                    System.exit(0);
                }
                case SAVE -> {
                    save(mainFile, toDoLists);
                }
                case HELP -> {
                    help();
                }
                case ADD -> {
                    var toDoFile = new File(entry.strip());
                    try {
                        if (toDoFile.createNewFile()) {
                            Use.printMessage("File created: " + toDoFile.getName());
                        } else {
                            Use.printMessage("File already exists: " + toDoFile.getName());
                            break;
                        }

                        toDoLists.add(entry.strip());
                        Use.printMessage("Successfully added.");
                        save(mainFile, toDoLists);
                    } catch (IOException e) {
                        System.out.println("An IOException occurred.");
                        e.printStackTrace();
                    }
                }
                case DEL -> {
                    int index;
                    if (entryScanner.hasNextInt())
                        index = entryScanner.nextInt();
                    else
                        break;

                    if (index >= toDoLists.size()) {
                        Use.printMessage("Given index was too large.");
                        break;
                    }

                    var delFileNmae = toDoLists.get(index);
                    var delFile = new File(delFileNmae);

                    try {
                        if (delFile.delete()) {
                            toDoLists.remove(index);
                            Use.printMessage("Successfully deleted item at index " + index + ".");

                            save(mainFile, toDoLists);
                        }
                    } catch (Exception e) {
                        Use.printMessage("Unable to delete file.");
                        e.printStackTrace();
                    }
                }
                case SET -> {
                    int index;
                    if (entryScanner.hasNextInt())
                        index = entryScanner.nextInt();
                    else
                        break;

                    if (index >= toDoLists.size() || index < 0) {
                        Use.printMessage("Invalid index.");
                        break;
                    }

                    String title;
                    if (entryScanner.hasNext())
                        title = entryScanner.nextLine().strip();
                    else
                        break;

                    var oldTitle = toDoLists.get(index);
                    var oldFile = new File(oldTitle);
                    var newFile = new File(title);
                    if (newFile.exists()) {
                        Use.printMessage("File with name " + newFile.getName() + " already exists.");
                        break;
                    }

                    if (oldFile.renameTo(newFile)) {
                        toDoLists.set(index, title);
                        Use.printMessage("Successfully set.");
                        save(mainFile, toDoLists);
                    }
                }
                case MOVE -> {
                    int oldIndex;
                    int newIndex;
                    if (entryScanner.hasNextInt()) {
                        oldIndex = entryScanner.nextInt();
                        if (entryScanner.hasNextInt()) {
                            newIndex = entryScanner.nextInt();
                            if (oldIndex >= toDoLists.size() || oldIndex < 0 ||
                                    newIndex >= toDoLists.size() || newIndex < 0) {
                                Use.printMessage("Invalid index.");
                                break;
                            }

                            var tmp = toDoLists.get(oldIndex);
                            toDoLists.set(oldIndex, toDoLists.get(newIndex));
                            toDoLists.set(newIndex, tmp);
                            Use.printMessage("Successfully moved.");
                        }
                    }
                }
                case LOAD -> {
                    int index;
                    if (entryScanner.hasNextInt())
                        index = entryScanner.nextInt();
                    else
                        break;

                    if (index >= toDoLists.size() || index < 0) {
                        Use.printMessage("Invalid index.");
                        break;
                    }

                    var toDoList = toDoLists.get(index);
                    ToDo.run(toDoList, userInput);
                }
            }
        }
    }

    private static void save(File mainFile, List<String> toDoLists) {
        try {
            Use.deleteContentOfFile(mainFile.getName());
            var bw = new BufferedWriter(new FileWriter(mainFile, true));
            bw.write(createOutputFromList(toDoLists));
            bw.close();
            Use.printMessage("Saved current list.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void help() {
        System.out.println("exit                    [save before exiting if you want to keep your changes]");
        System.out.println("save");
        System.out.println("add {title}");
        System.out.println("del {id}                [delete entries]");
        System.out.println("set {id} {title}        [set new entry titles]");
        System.out.println("move {id} {position}    [move entry to position]");
        System.out.println("load {id}               [load a To-Do-List]");
        Use.printMessage("Values for {id} are Integers, e.g. 0, 1, 2, 55, 666, 9999.");
        Use.printMessage("Examples:\nadd Shopping List\nmove 0 3\ndel 1");
    }

    private static List<String> buildListFromFile(File file) {
        var result = new ArrayList<String>();

        try {
            var br = new BufferedReader(new FileReader(file));
            String str;

            while ((str = br.readLine()) != null) {
                result.add(str);
            }

        } catch (IOException e) {
            Use.printMessage("Unable to build list from file \"" + file.getName() + "\".");
            e.printStackTrace();
        }

        return result;
    }

    private static String createOutputFromList(List<String> list) {
        var result = new StringBuilder();
        list.forEach(item -> result.append(item).append('\n'));
        return result.substring(0);
    }

    private static void displayList(List<String> list) {
        System.out.println();
        if (list.isEmpty()) {
            Use.printMessage("List is empty...");
        } else {
            Use.printListHeader("Main", standardTitleLength);
            var lengthOfLastIndex = Use.getNumberOfDigits(list.size()-1);

            for (int i = 0; i < list.size(); i++) {
                var lengthOfCurrentIndex = Use.getNumberOfDigits(i);
                var indexIndentationDiff = lengthOfLastIndex - lengthOfCurrentIndex;
                Use.printIndentation(indexIndentationDiff);
                System.out.println(i + ". " + list.get(i));
            }
        }
        System.out.println();
    }
}
