import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Spliterator;

public class Save {
    private static final String COMMA = ",";
    private static final String NEW_LINE = "\n";
    private static final String SPACE = " ";

    public static ToDoList buildToDoListFromFile(String fileName, boolean showPos, boolean showTimePercentage, int posGap) {
        var result = new ToDoList(showPos, showTimePercentage, posGap);
        var list = new ArrayList<ToDoEntry>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));

            String str;
            int posCounter = 0;

            while ((str = br.readLine()) != null) {
                if (str.matches("(true|false)")) {
                    boolean done = Boolean.parseBoolean(str);
                    String title = br.readLine();
                    String details = br.readLine();

                    ToDoEntry nextEntry = new ToDoEntry(posCounter, title, done);

                    nextEntry.details = processDetailsFromFile(details);

                    list.add(nextEntry);

                    posCounter++;
                } else if (str.startsWith("hidden")) {
                    String[] hidden = str.substring(6).split(COMMA);
                    for (String h : hidden) {
                        int i;

                        try {
                            i = Integer.parseInt(h);
                        } catch (NumberFormatException e) {
                            continue;
                        }

                        list.get(i).hidden = true;
                    }
                } else if (str.startsWith("timed")) {
                    String[] timed = str.substring(5).split(COMMA);
                    for (String t : timed) {
                        Scanner timeScanner = new Scanner(t);
                        int i = -1;
                        if (timeScanner.hasNextInt())
                            i = timeScanner.nextInt();

                        long seconds = 0;
                        if (timeScanner.hasNextLong())
                            seconds = timeScanner.nextLong();

                        if (i < 0 || i >= list.size() || seconds == 0) {
                            timeScanner.close();
                            continue;
                        }

                        list.get(i).timeRequired = Duration.ofSeconds(seconds);
                        timeScanner.close();
                    }
                } else if (str.startsWith("timespent")) {
                    String[] timesSpent = str.substring(9).split(COMMA);
                    for (String t : timesSpent) {
                        Scanner timeScanner = new Scanner(t);
                        int i = -1;
                        if (timeScanner.hasNextInt())
                            i = timeScanner.nextInt();

                        long seconds = 0;
                        if (timeScanner.hasNextLong())
                            seconds = timeScanner.nextLong();

                        if (i < 0 || i >= list.size() || seconds == 0) {
                            timeScanner.close();
                            continue;
                        }

                        list.get(i).timeSpent = Duration.ofSeconds(seconds);
                        timeScanner.close();
                    }

                } else if (str.startsWith("deadline")) {
                    String line = str.substring(8);
                    Scanner sc = new Scanner(line);
                    int i = -1;
                    if (sc.hasNextInt())
                        i = sc.nextInt();

                    list.get(i).deadline = sc.nextLine().strip();
                    sc.close();
                } else if (str.startsWith("collapsed")) {
                    String[] collapsed = str.substring(9).split(COMMA);
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).collapsed = Boolean.parseBoolean(collapsed[i]);
                    }
                } else if (str.startsWith("showPos")) {
                    result.showPositions = Boolean.parseBoolean(str.substring(7));
                } else if (str.startsWith("posGap")) {
                    result.positionGap = Integer.parseInt(str.substring(6));
                } else if (str.startsWith("showTimePercentage")) {
                    result.showTimePercentage = Boolean.parseBoolean(str.substring(18));
                } else if (str.startsWith("constant")) {
                    String[] constant = str.substring(8).split(COMMA);
                    for (String h : constant) {
                        int i;
                        try {
                            i = Integer.parseInt(h);
                        } catch (NumberFormatException e) {
                            continue;
                        }

                        list.get(i).constant = true;
                    }
                } else if (str.startsWith("counters=")) {
                    String[] counters = str.substring(9).split(COMMA);
                    for (String counter : counters) {
                        Scanner sc = new Scanner(counter);
                        int i = -1;
                        if (sc.hasNextInt())
                            i = sc.nextInt();

                        int c = -1;
                        if (sc.hasNextInt())
                            c = sc.nextInt();

                        if (i >= 0 && i < list.size() && c > -1)
                            list.get(i).counter = c;

                        sc.close();
                    }
                } else if (str.startsWith("maxcounters=")) {
                    String[] counters = str.substring(12).split(COMMA);
                    for (String counter : counters) {
                        Scanner sc = new Scanner(counter);
                        int i = -1;
                        if (sc.hasNextInt())
                            i = sc.nextInt();

                        int c = -1;
                        if (sc.hasNextInt())
                            c = sc.nextInt();

                        if (i >= 0 && i < list.size() && c > -1)
                            list.get(i).maxCounter = c;

                        sc.close();
                    }
                } else {
                    Use.printMessage("Specified file already contains other unsuitable content!");
                    return null;
                }
            }
            br.close();

        } catch (IOException ioe) {
            Use.printMessage("Unable to build ToDo-List from file \"" + fileName + "\".\nExiting...");
            ioe.printStackTrace();
            return null;
        }

        result.list = list;
        return result;
    }

    private static String processDetailsFromFile(String details) {
        StringBuilder detailsBuilder = new StringBuilder();
        if (!details.isEmpty()) {
            String[] detailsLines = details.split("\\$%\\$");
            for (String line : detailsLines) {
                detailsBuilder.append(line).append(NEW_LINE);
            }
        }
        return detailsBuilder.substring(0);
    }

    public static String createToDoListSaveString(List<ToDoEntry> toDoList, boolean showPos, boolean showTimePercentage, int posGap) {
        StringBuilder result = new StringBuilder();
        List<Integer> hidden = new ArrayList<>();
        List<Integer> timed = new ArrayList<>();
        List<Integer> timesSpent = new ArrayList<>();
        List<Integer> constant = new ArrayList<>();
        List<Integer> deadlines = new ArrayList<>();
        List<Integer> counters = new ArrayList<>();
        List<Integer> maxCounters = new ArrayList<>();

        for (ToDoEntry entry : toDoList) {
            StringBuilder detailsBuilder = new StringBuilder();

            if (!entry.details.isEmpty()) {
                String[] detailsLines = entry.details.split(NEW_LINE);
                for (String line : detailsLines) {
                    detailsBuilder.append(line).append("$%$");
                }
            }

            if (entry.hidden)
                hidden.add(entry.position);

            if (!entry.timeRequired.equals(Duration.ZERO))
                timed.add(entry.position);

            if (!entry.timeSpent.equals(Duration.ZERO))
                timesSpent.add(entry.position);

            if (entry.constant)
                constant.add(entry.position);

            if (entry.deadline.length() > 0)
                deadlines.add(entry.position);

            if (entry.counter >= 0)
                counters.add(entry.position);

            if (entry.maxCounter >= 0)
                maxCounters.add(entry.position);

            result.append(entry.done).append(NEW_LINE).append(entry.title).append(NEW_LINE).append(detailsBuilder.substring(0)).append(NEW_LINE);
        }

        result.append("hidden");
        for (Integer h : hidden)
            result.append(h).append(COMMA);

        result.append("\ntimed");
        for (Integer t : timed)
            result.append(t).append(SPACE).append(toDoList.get(t).timeRequired.toSeconds()).append(COMMA);

        result.append("\ntimespent");
        for (Integer t : timesSpent)
            result.append(t).append(SPACE).append(toDoList.get(t).timeSpent.toSeconds()).append(COMMA);

        result.append("\nconstant");
        for (Integer c : constant)
            result.append(c).append(COMMA);

        for (Integer d : deadlines)
            result.append("\ndeadline").append(d).append(SPACE).append(toDoList.get(d).deadline);

        result.append("\ncollapsed");
        for (ToDoEntry entry : toDoList)
            result.append(entry.collapsed).append(COMMA);

        result.append("\ncounters=");
        for (Integer c : counters)
            result.append(c).append(SPACE).append(toDoList.get(c).counter).append(COMMA);

        result.append("\nmaxcounters=");
        for (Integer c : maxCounters)
            result.append(c).append(SPACE).append(toDoList.get(c).maxCounter).append(COMMA);

        result.append("\nshowPos").append(showPos);
        result.append("\nposGap").append(posGap);
        result.append("\nshowTimePercentage").append(showTimePercentage);
        return result.substring(0);
    }
}
