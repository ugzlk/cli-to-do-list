import java.io.FileWriter;
import java.io.IOException;

public class Use {
    public static void printMessage(String message) {
        System.out.println("\n"+message+"\n");
    }

    public static void printListHeader(String fileName, int standardTitleLength) {
        System.out.println("(" + fileName + ") To-Do:");
        int n = fileName.length() + standardTitleLength;
        printRepeatedString("=", n);
        System.out.print("\n");
    }

    public static void printIndentation(int length) {
        if (length > 0) {
            String space = " ";
            System.out.print(space.repeat(length));
        }
    }

    public static String getIndentation(int length) {
        String result = "";
        if (length > 0) {
            String space = " ";
            result = space.repeat(length);
        }
        return result;
    }

    public static void printDetailsSeparator(int titleLength, int indentation) {
        printIndentation(indentation);
        printRepeatedString("-", titleLength);
        System.out.print("\n");
    }

    public static void printRepeatedString(String s, int repeat) {
        System.out.print(s.repeat(repeat));
    }

    public static int getNumberOfDigits(int number) {
        int length = 1;
        while (number >= 10) {
            number /= 10;
            length++;
        }
        return length;
    }

    public static Command parseCommand(String input) throws IllegalArgumentException {
        var result = Command.valueOf(input.toUpperCase());
        return result;
    }

    public static void deleteContentOfFile(String fileName) {
        try {
            new FileWriter(fileName, false).close();
        } catch (IOException e) {
            Use.printMessage("Unable to delete content of file with name \"" + fileName + "\"\nExiting...");
            System.exit(1);
        }
    }
}
